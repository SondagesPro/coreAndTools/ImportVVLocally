# ImportVVLocally

Import a VV file with PHP CLI. Avoid big file upload, avoid time out when import.

## Usage

`php application/commands/console.php plugin --target=ImportVVLocally -f filename -s surveyid[ -e existingId][ -l firstline]`
see LimeSurvey manual for detail https://manual.limesurvey.org/Import_responses#Import_a_VV_survey_file 
- filename : full file path to the tab separated value in UTF8 format as VV file
- surveyid : active survey id to import the VV file
- existingId : what to do in case of existing id : 
  - ignore : remove value before import (default)
  - replace : replace whole response by response in file
  - replaceanswers : update response by reponse in the file, keep other columns
  - skip : skip the response in the file
  - renumber : create an new response
- firstline : shearder are at line 2 : default to true 


If you use LimeSurvey version before 3.17.8, you need to be in LimeSurvey directory for call console.php.

## Home page, copyright and support
- HomePage <https://extensions.sondages.pro/>
- Copyright © 2022 Denis Chenu <https://sondages.pro>
- Licence : GNU Affero General Public License <https://www.gnu.org/licenses/agpl-3.0.html>
- Issues and merge request on [Gitlab](https://gitlab.com/SondagesPro/coreAndTools/backupSurveyLocally/merge_requests)
- [Professional support](https://support.sondages.pro/)
