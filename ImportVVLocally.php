<?php
/**
 * ImportVVLocally : Import VV local VV file with PHP CLI
 *
 * @author Denis Chenu <denis@sondages.pro>
 * @copyright 2022 Denis Chenu <https://www.sondages.pro>
 * @license AGPL v3
 * @version 0.1.0
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

class ImportVVLocally extends PluginBase
{
    protected $storage = 'DbStorage';
    static protected $description = 'Import VV local VV file with PHP CLI';
    static protected $name = 'ImportVVLocally';

    /* @var string */
    private $_errorOnDir = "";

    /**
    * Add function to be used in cron event
    * @see parent::init()
    */
    public function init()
    {
        /* Action on cron */
        $this->subscribe('direct','importVV');
    }

    /**
     * The real action of this plugin, find all survey to be saved
     * Renaming dir one by one
     * Save surveys as LSA in the #1
     */
    public function importVV()
    {
        if (!$this->getEvent()) {
            throw new CHttpException(403);
        }
        if($this->event->get("target") != get_class()) {
            return;
        }
        /* can not use getopts, then get by SERVER */
        $argsv = isset($_SERVER['argv']) ? $_SERVER['argv']:array();
        if (empty($argsv)) {
            $this->sendUsage();
        }
        $startLine = 1;
        $existingId = "ignore";
        foreach($argsv as  $key => $argv) {
            if($argv == "-f" && isset($argsv[$key+1])) {
                $file = $argsv[$key+1];
            }
            if($argv == "-s" && isset($argsv[$key+1])) {
                $surveyid = $argsv[$key+1];
            }
            if($argv == "-l" && isset($argsv[$key+1])) {
                $deleteFirstLine = boolval($argsv[$key+1]);
            }
            if($argv == "-e" && isset($argsv[$key+1])) {
                $existingId = $argsv[$key+1];
            }
        }
        if(empty($file) || empty($surveyid)) {
            $this->sendUsage();
        }
        $this->LsCommandFix();
        $oSurvey =  Survey::model()->findByPk($surveyid);
        if (empty($oSurvey)) {
            $this->sendError("No survey $surveyid");
        }
        if ($oSurvey->active != "Y") {
            $this->sendError("Survey $surveyid not activated");
        }
        if (!@is_readable($file)) {
            $this->sendError("File $file unreadable");
        }
        $fileInfo = pathinfo($file);
        $extension = strtolower($fileInfo['extension']);
        if (!in_array($extension, ['csv','tsv','txt','vv'])) {
            $this->sendError("Bad filetype for $file, only csv, txt, tsv and vv are allowed");
        }
        /* need to move file */
        $newfilename = App()->getConfig("tempdir") . DIRECTORY_SEPARATOR . "upload" . DIRECTORY_SEPARATOR . "ImportVVLocally_" . $fileInfo['basename'];
        if (@!copy($file, $newfilename)) {
            $this->sendError("Uname to copy $file to temp/upload directory (" . App()->getConfig("tempdir") . ")");
        }
        Yii::import('application.helpers.admin.import_helper', true);
        Yii::import('application.helpers.common_helper', true);
        Yii::import('application.helpers.surveytranslator_helper', true);
        Yii::import('application.helpers.replacements_helper', true);
        Yii::import('application.helpers.expressions.em_manager_helper', true);
        $options = array(
            'bDeleteFistLine' => $deleteFirstLine,
            'sExistingId' => $existingId,
            'bForceImport' => false,// needed in case
        );
        $result = CSVImportResponses($newfilename, $surveyid, $options);
        $exit = 0;
        if(isset($result['success'])) {
            echo "Success \n";
            foreach ($result['success'] as $success) {
                echo "    {$success}\n";
            }
        }
        if(isset($result['errors'])) {
            echo "Error\n";
            foreach ($result['errors'] as $error) {
                echo "    {$error}\n";
            }
            $exit = 2;
        }
        if(isset($result['warnings'])) {
            echo "Warning \n";
            foreach ($result['warnings'] as $warning) {
                echo "    {$warning}\n";
            }
        }
        exit($exit);
    }

    /**
     * Bad usage : send usafe
     * exit 1
     */
    public function sendUsage()
    {
        echo "Usage: application/commands/console.php plugin --target=ImportVVLocally -f filename -s surveyid[ -e existingId][ -l firstline]\n";
        echo "    see LimeSurvey manual for detail https://manual.limesurvey.org/Import_responses#Import_a_VV_survey_file \n";
        echo "    - filename : full file path to the tab separated value in UTF8 format as VV file\n";
        echo "    - surveyid : active survey id to import the VV file\n";
        echo "    - existingId : what to do in case of existing id : \n";
        echo "      - ignore : remove value before import (default)\n";
        echo "      - replace : replace whole response by response in file\n";
        echo "      - replaceanswers : update response by reponse in the file, keep other columns\n";
        echo "      - skip : skip the response in the file\n";
        echo "      - renumber : create an new response\n";
        echo "    - firstline : shearder are at line 2 : default to true \n";
        exit(1);
    }

    /**
     * Error on params
     * exit 2
     */
    public function sendError($string)
    {
        echo "Error: {$string}\n";
        exit(2);
    }

    /**
     * @see parent::gT for LimeSurvey 3.0
     * @param string $sToTranslate The message that are being translated
     * @param string $sEscapeMode unescaped by default
     * @param string $sLanguage use current language if is null
     * @return string
     */
    private function _translate($sToTranslate, $sEscapeMode = 'unescaped', $sLanguage = null)
    {
        if(is_callable($this, 'gT')) {
            return $this->gT($sToTranslate,$sEscapeMode,$sLanguage);
        }
        return $sToTranslate;
    }

    /**
     * Fix LimeSurvey command function and add own needed function 
     * @todo : find way to control API
     * OR if another plugin already fix it
     */
    private function LsCommandFix()
    {
        /* Bad autoloading in command */
        if (!class_exists('DbStorage', false)) {
            include_once(dirname(__FILE__)."/DbStorage.php");
        }
        if (!class_exists('ClassFactory', false)) {
            Yii::import('application.helpers.ClassFactory');
        }

        ClassFactory::registerClass('Token_', 'Token');
        ClassFactory::registerClass('Response_', 'Response');

        /** needed for rmdirr function **/
        Yii::import('application.helpers.common_helper', true);
        /* Replace getGlobalSetting core function (never save global settings when load it)*/
        Yii::setPathOfAlias(get_class($this), dirname(__FILE__));
        Yii::import(get_class($this).'.helpers.globalsettings_helper', true);
        /** Needed function for export **/

        $defaulttheme = Yii::app()->getConfig('defaulttheme');
        /* Bad config set for rootdir */
        if( !is_dir(Yii::app()->getConfig('standardthemerootdir').DIRECTORY_SEPARATOR.$defaulttheme) && !is_dir(Yii::app()->getConfig('userthemerootdir').DIRECTORY_SEPARATOR.$defaulttheme)) {
            /**
             * if still broken : throw error : risk of updating survey when load
             * @see : https://gitlab.com/SondagesPro/mailing/sendMailCron/-/commit/31165bf12201e3dc82b927df095a8f5221bb73d3
             **/
            throw new CException("Unable to find default theme {$defaulttheme} in current theme directory");
        }
        App()->session['adminlang'] = App()->getConfig("defaultlang");
    }
}
